// dependencies
const async = require('async');
const AWS = require('aws-sdk');
const util = require('util');
const csv = require("fast-csv");
const uuidv1 = require('uuid/v1');

const process = require('process');

const filter = require("stream-filter");
const S3S = require('s3-streams');

// get reference to S3 client 
const s3 = new AWS.S3();


exports.handleFile = function(event, context, callback) {
    // Read options from the event.
    console.log("Reading options from event:\n", util.inspect(event, {depth: 5}));

    const srcBucket = event.Records[0].s3.bucket.name;
    // Object key may have spaces or unicode non-ASCII characters.
    const srcKey    = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " "));
    const dstBucket = process.env.DESTINATION_BUCKET;
    const dstKey    = srcKey + ".json";

    stream(srcBucket,srcKey,dstBucket,dstKey,callback);
};


function stream(srcBucket,srcKey,destBucket,destKey,callback) {
    //let s3 = new AWS.S3();
    let readParams = {Bucket: srcBucket, Key: srcKey};
    let readStream = s3.getObject(readParams).createReadStream();

    let writeParams =  {Bucket: destBucket, Key: destKey};
    let writeStream = S3S.WriteStream(s3,writeParams,{});


    csv.fromStream(readStream, {headers : true,trim: true,objectMode:true})
        .validate(function(rowString) {
            let row = JSON.parse(rowString);
            return row.hasOwnProperty("id") &&
                row.hasOwnProperty("last_name") &&
                row.hasOwnProperty("first_name") &&
                row.hasOwnProperty("academic_level") &&
                row.hasOwnProperty("email") &&
                row.hasOwnProperty("city") &&
                row.hasOwnProperty("state");
        })
        .transform(function(dataRow) {
            //the output should be in 'hadoop' style event stream JSON format
            //individual JSON maps, separated by new lines.
            return JSON.stringify(dataRow)+"\n";
        })
        .on("data-invalid", function(data){
            //do something with invalid row
            console.error("Invalid Row",data.toString());
        })
        .on("end", function(){
            console.log("done");
            callback();
        })
        .pipe(writeStream,{smart: true});
}