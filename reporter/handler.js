"use strict";

const AthenaExpress = require("athena-express"),
    aws = require("aws-sdk");

const s3 = new aws.S3({params: {Bucket: 'psp-website'}});

/* AWS Credentials are not required here
/* because the IAM Role assumed by this Lambda
/* has the necessary permission to execute Athena queries
/* and store the result in Amazon S3 bucket */

const athenaExpressConfig = {
  aws,
  s3:"s3://psp-athena-results",
  db: "presentationdb",
  getStats: true
};
const athenaExpress = new AthenaExpress(athenaExpressConfig);


//dbcreate(athenaExpress);

exports.generatechart = async (event, context, callback) => {

const sqlQuery = `
  SELECT count(Id) AS Count, Academic_level
FROM people
GROUP BY  Academic_level
ORDER BY  Count desc, Academic_level;
  `;

  try {
      console.log("Running query:",sqlQuery);
      await athenaExpress.query(sqlQuery).then(results => {

          let chartData = {
              labels: [],
              data: [],
              dataMap:{}
          };
          results.Items.forEach(item => {
              console.log("Item",item);
            chartData.labels.push(item.Academic_level);
            chartData.data.push(item.Count);
            chartData.dataMap[item.Academic_level] = item.Count;
          });
          let data = JSON.stringify(chartData);
          console.log("Results",data);
          let params = {
              Bucket: 'psp-website',
              ACL: 'public-read',
              Key: 'chartData.json',
              Body: data,
              ContentType: 'application/json'
          };
          return s3.upload(params).promise();

      }).then(s3Result => {
            console.log("Upload Result:",s3Result);
            callback(null,s3Result);
      }).catch(error => {
          console.log("Error",error);
          callback(error, null);
      });

  } catch (error) {
    callback(error, null);
  }
};


function dbcreate(athenaExpress) {
  const ddlStatement = `
  CREATE EXTERNAL TABLE IF NOT EXISTS people (
    Id STRING,
    First_Name STRING,
    Last_Name STRING,
    Email STRING,
    Address STRING,
    City STRING,
    State STRING,
    Academic_Level STRING,
    Zip_Code STRING,
    IP_Address STRING,
) ROW FORMAT SERDE 'org.apache.hive.hcatalog.data.JsonSerDe' STORED AS TEXTFILE
    LOCATION 's3://psp-processed-data/';
`;

  try {
    let results =  athenaExpress.query(ddlStatement);
    callback(null, results);
  } catch (error) {
    callback(error, null);
  }
}
