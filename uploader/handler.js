'use strict';

const AWS = require('aws-sdk');
const querystring = require('querystring');

//*/ get reference to S3 client
var s3 = new AWS.S3();
// Set this to the region you upload the Lambda function to.
AWS.config.region = 'us-west-2';

exports.handler = function(evt, context, callback) {
  // Our raw request body will be in evt.body.
  const params = querystring.parse(evt.body);

  // Our file field from the request.
  const my_file_field = params['data-file'];

  const my_name = params['username'];



  // Generate HTML.
  const html = `<!DOCTYPE html><p>You said: ` + my_field + `</p>`;

  // Return HTML as the result.
  callback(null, html);
};