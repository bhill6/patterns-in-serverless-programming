#!/usr/bin/env bash


aws ecs create-cluster --cluster-name fargate-cluster

aws ecs register-task-definition --cli-input-json file://generate-pdf.json  --region us-east-1
