#!/usr/bin/env sh

export WIDTH=400;

curl -o /tmp/chart.json http://psp-website.s3.amazonaws.com/chartData.json


export MAX=$(cat /tmp/chart.json |jq  -r  '(.data[0] | tonumber)')

export SEN_SIZE=$(cat /tmp/chart.json |jq  -r  "((.dataMap.Senior | tonumber) / $MAX) * $WIDTH" )
export JUN_SIZE=$(cat /tmp/chart.json |jq  -r  "((.dataMap.Junior | tonumber) / $MAX) * $WIDTH")
export SOP_SIZE=$(cat /tmp/chart.json |jq  -r  "((.dataMap.Sophomore | tonumber) / $MAX) * $WIDTH")
export FRE_SIZE=$(cat /tmp/chart.json |jq  -r  "((.dataMap.Freshman | tonumber) / $MAX) * $WIDTH")

export SEN_TOTAL=$(cat /tmp/chart.json |jq  -r  "(.dataMap.Senior | tonumber)" )
export JUN_TOTAL=$(cat /tmp/chart.json |jq  -r  "(.dataMap.Junior | tonumber)")
export SOP_TOTAL=$(cat /tmp/chart.json |jq  -r  "(.dataMap.Sophomore | tonumber)")
export FRE_TOTAL=$(cat /tmp/chart.json |jq  -r  "(.dataMap.Freshman | tonumber)")




printf "SEN: $SEN_TOTAL\n";
printf "JUN: $JUN_TOTAL\n";
printf "SOP: $SOP_TOTAL\n";
printf "FRE: $FRE_TOTAL\n";


envtpl ./template.html > /tmp/out.html

wkhtmltopdf  --page-height 50 --page-width 100  /tmp/out.html /tmp/out.pdf

curl https://slack.com/api/files.upload -F token="${SLACK_TOKEN}" -F channels="${SLACK_CHANNEL}" -F title="Upload Update" -F filename="uploaded.pdf" -F file=@"/tmp/out.pdf"
